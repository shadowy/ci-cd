#!/bin/bash
HASH=($(md5sum ./yarn.lock))
CACHE="cache/${CI_PROJECT_PATH}/${HASH}"
echo "HASH: ${HASH}"
echo "CACHE: ${CACHE}"
if [ -d "/cache" ]; then
  if [ ! -d "/cache/${CACHE}/node_modules" ]; then
    echo "CACHE create folder"
    mkdir -p /cache/${CACHE}
    echo "CACHE yarn install"
    yarn install
    echo "CACHE save"
    cp -r ./node_modules /cache/${CACHE}/node_modules
  else
    echo "CACHE restore"
    cp -r /cache/${CACHE}/node_modules ./node_modules
  fi
else
  yarn install
fi
