#!/bin/bash
HASH=($(md5sum ./go.mod))
CACHE="cache/${CI_PROJECT_PATH}/${HASH}"
echo "HASH: ${HASH}"
echo "CACHE: ${CACHE}"
if [ -d "/cache" ]; then
  if [ ! -d "/cache/${CACHE}/vendor" ]; then
    echo "CACHE create folder"
    mkdir -p /cache/${CACHE}
    echo "CACHE go mod vendor"
    go mod vendor
    echo "CACHE save"
    cp -r ./vendor /cache/${CACHE}/vendor
  else
    echo "CACHE restore"
    cp -r /cache/${CACHE}/vendor ./vendor
  fi
else
  go mod vendor
fi
