# CHANGELOG

<!--- next entry here -->

## 0.24.0
2022-09-19

### Features

- update golang image (5dc0b367dedcabd8894d21e986e9891d63c53619)

## 0.23.0
2022-05-09

### Features

- update golang image (561999621e2e0eb22e5d3eddb23fa157e321283b)

## 0.22.0
2021-12-17

### Features

- update golang image (fbe854257ba7aae0b600815f431dec4c7f0a8ced)

## 0.21.0
2021-12-06

### Features

- update golang image (ef5f8741ae7e458efb7a386d413854a555c760ba)

## 0.20.0
2021-12-02

### Features

- update image for golang lib (895e847a3da2fc449762061e9784e4fe53ac0567)

## 0.19.0
2021-12-02

### Features

- update image for golang lib (64daf3a7b4e87ffea81255fd6f37df2d5f04253a)

## 0.18.0
2021-12-02

### Features

- update image for golang lib (2f5bce4a91b19ffb114d3c9d2ec8bc5475f6dd00)

## 0.17.0
2021-12-01

### Features

- update image for golang lib (3337af3a4925cbfb180510500eb9d4a034c5b5d6)

## 0.16.0
2021-12-01

### Features

- add build docker for docker hub (35d6ca9098ecc9b47a9e081162fb8538942592ee)

## 0.15.0
2021-08-26

### Features

- add main branch (6cc1c6d3fbd58af39766f736ef8e210cacc84e56)

## 0.14.0
2021-05-24

### Features

- add version for go projects (7f84a2b333c38b2a0a2b22820aba9b1a778c3c9e)

## 0.13.0
2021-05-14

### Features

- add node 14 (8a013a4dd48f1a009be3232a5ae08841637c6219)

## 0.12.2
2021-03-29

### Fixes

- change environment variables (d74ee152a5e82ca01ffdd927b552db1d9791f34b)

## 0.12.1
2021-03-29

### Fixes

- change environment variables (5eaa378dbe2e9a72384212bdc28b6d4a716f9365)

## 0.12.0
2021-03-29

### Features

- add new flow for deployment (6260bc094a2d5a20918993093edc19039bf95296)

## 0.11.0
2020-11-13

### Features

- add flow for node:15 (6dd2e452b1cfac9d3aac2cc73fa02f25a8749c0a)

## 0.10.0
2020-09-18

### Features

- add ci/cd for nx.dev (f43f6e018b77310fc8cdd736894206dbb42f078e)

## 0.9.1
2020-09-01

### Fixes

- add container_image_suffix (0bb824ef990fbfb852bd1d04f9e185adc13aba95)

## 0.9.0
2020-09-01

### Features

- add container_image_suffix (e746bfa0cbcfdf6bee5262c48a6e7fbb06eb1a18)

## 0.8.0
2020-08-27

### Features

- add nestjs-gitlab-ci.yml (46f3919247fdda3483fa5cd0c7b23d3fb98218a9)

## 0.7.10
2020-08-27

### Fixes

- for debug (636ca359a3eea4d1db15ba86cb3e6140a1c8e319)

## 0.7.9
2020-08-27

### Fixes

- for debug (419fc95b43ab3dd23dd409d3b148f52be747343c)

## 0.7.8
2020-06-25

### Fixes

- fix version of nodejs (2dd6877ae6c91b61b88121905a7f454713626e67)

## 0.7.7
2020-06-11

### Fixes

- fix ci issue (d7d4d51286c4ecf72cdf32b2d17cc5b0b57bfa0c)

## 0.7.6
2020-06-10

### Fixes

- refactoring ci (ae30ed4e8c9b958c526b4538d9f67d8075fdb4af)
- refactoring ci (6e104d48eeb8df1afe353e37228b5017bb33b3b5)

## 0.7.5
2020-06-05

### Fixes

- fix Dockerfile (c8e07cd26c6938b0ec1846f377aee264054ef4dc)
- fix Dockerfile (f87cd3d2d9dac223f1ecefd3ff8d00913e1a4aaf)

## 0.7.4
2020-06-05

### Fixes

- add debug info (5996c8bd832d0e2b847b482de03864cfda14cc52)

## 0.7.3
2020-06-05

### Fixes

- update go lang (3ab49b01104e5178a30402f21f7d9919b9ea3732)

## 0.7.2
2020-06-05

### Fixes

- update go lang start.sh (0b3450ff6ad5a146934f2a78c0b97219496307c5)

## 0.7.1
2020-05-28

### Fixes

- fix Dockerfile for go (08165e80de9dc01717f77246609e243b4e9365f7)

## 0.7.0
2020-05-28

### Features

- add go service (35c6d3fd591e281bc90336014ed26d58cdaaadd0)

## 0.6.12
2020-05-26

### Fixes

- fix ui (2b016a97c13bfc41591382a8a757448b2c7bc570)

## 0.6.11
2020-05-26

### Fixes

- fix ui (8af360ae9ccfce7fc79d6d7e0fab8f06dc397e49)

## 0.6.10
2020-05-26

### Fixes

- fix ui (69fbdd0b09a984e2a27547b85f5dc9f75cbd7bc3)

## 0.6.9
2020-05-26

### Fixes

- fix ui (0e561ecf1470e95681ade917bb586c302636ae31)

## 0.6.8
2020-05-26

### Fixes

- fix ui (7f70a060b9d4b3ff5a05a9ae8e45a32425034aa6)

## 0.6.7
2020-05-26

### Fixes

- fix ui (548f74be3ffb0386ba38d3b1c5b8a3fbb27a22e6)

## 0.6.6
2020-05-26

### Fixes

- fix ui (b34d3bd696780a53945f51373642804289f6e11d)
- fix ui (a92912a6791abd189de271df08012ca098852094)

## 0.6.5
2020-05-26

### Fixes

- fix ui (eff355d8f956ed2201ecedb486f7ddd626c89c4a)

## 0.6.4
2020-05-26

### Fixes

- fix ui (a47654077ea31d9e3a10082e47012f0403962602)

## 0.6.3
2020-05-26

### Fixes

- fix ui (20fb4c0de60f16903a4300559799aa9e51201c20)

## 0.6.2
2020-05-26

### Fixes

- fix ui (63cc53ec5e5a33ec0a31c7b0b355d27284d6131c)

## 0.6.1
2020-05-26

### Fixes

- fix ui (b0c9ad606ccdebd75001d2e2ef3b5c4de13bd112)

## 0.6.0
2020-05-26

### Features

- add ui (c2cc2e335ec89b4042660cc0372e72b1a5502b5f)

## 0.5.0
2020-05-26

### Features

- add ui (2ae69e305eb8ae6271623bf302600470e999dce5)

## 0.4.0
2020-05-26

### Features

- add ui (5c6eaf659752b58179228c7eeada161304dcd4dc)

## 0.3.0
2020-05-26

### Features

- add bus (55fb61b1707b3fcef090bbefeebef6d92029e647)

## 0.2.0
2020-05-26

### Features

- add db (17fdd8b521b00c5e2bc46f6cdc22f779daa41aa8)

### Fixes

- fix db (5517471320c6ece46ea5c458ad6f41a79d01e9c2)
- fix db (a72598fdb814da73ca20e888c8d15ecdd6f167c8)

## 0.1.4
2020-04-29

### Fixes

- update main ci (31a1567a9edb41e47dfbdca87c18512f99735fd1)

## 0.1.3
2020-04-29

### Fixes

- update main ci (b6de67d6c240c53842aa0fc9a0f997ec71006fa5)

## 0.1.2
2020-04-29

### Fixes

- update main ci (fb3110910a4e424ae40ed766d71e1a11d70e6e21)

## 0.1.1
2020-04-29

### Fixes

- golang-lib (06e1133ab0601275b39bfac5f38b131e15bf5b1c)

## 0.1.0
2020-04-29

### Features

- test (641b2f2ae0fdf38fb7d875fde4906f14483639e9)
- test (8a8611828e4acb051eb91a40e97e34028d983937)
- test (853e8d99d14ee33dc2cba21dfbcd7a44f6606139)
- test (897aaa28ab887795ad96f1df53b9c5bcf13bc8d8)
- test (6378c63fca5b629890dd22bb08cf6e10b789b70c)
- test (f0332b21e35d1137966d79953d5c4d249fa5d5d0)
- test (ee3881776e1de57635d2071cce639c84daf40f94)
- test (77a432066717db05f5eadce40ec5ba858ad21d29)
- basic structure (e41a6d3575562985b9ff0787f29db3ebf3a5a917)
- golang-lib (dbb0dc69867b615868b735d6e08e629e19633406)

### Fixes

- test (33a62e8ed0434a2fcd23c25160180873294d0ace)
- test (7bf89d9e3211c668fb82ecb4a2c9fc730ff054ce)
- basic script (67ff69256251b0eb4774d07267c055ba12a7e5c1)
- basic script (eb76484dd4ec0727f1cfc78a78809883ce530f86)
- basic script (83ef5d77d0a3cd580a111b02a8eda18d1991c8f9)
- basic script (e4e4f46646cc75ea99745ab529e14ba25238a335)
- basic script (e3f73a4992be77edb4b90262bc1b01cb03a2212b)